import datetime
import os
from test_news import settings


# create unique filename
def generate_unique_name(instance, filename):
    filename, file_extension = os.path.splitext(filename)
    return "news-photos/{}{}".format(str(datetime.datetime.now()),
                                     file_extension)
