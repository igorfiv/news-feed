from django.test import TestCase
from django.urls import reverse


# Create your tests here.
class NewsTestCase(TestCase):

    def test_news_page_load(self):
        response = self.client.get(reverse('news'))
        self.assertEqual(response.status_code, 200)

    def test_news_detail_page_load(self):
        response = self.client.get(reverse('news_detail', kwargs={'news_id': 1}))
        print(reverse('news_detail', kwargs={'news_id': 1}))
        self.assertEqual(response.status_code, 200)
