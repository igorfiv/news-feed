from django.shortcuts import render
from django.contrib import messages

from .models import News


# Create your views here.
def news(request):
    template = 'news.html'

    all_public_news = News.objects.filter(is_public=True).order_by('public_date')
    context = {'news': all_public_news}

    return render(request, template, context)


def news_detail(request, news_id):
    template = 'new-detail.html'
    one_news = News.objects.filter(id=news_id)
    context = {'one_news': one_news}

    if one_news:
        return render(request, template, context)
    else:
        messages.info(request, message='The news with entered ID not found')
        return render(request, template, context)
