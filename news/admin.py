from django.db import models
from django.contrib import admin
from django.forms import TextInput, Textarea
from .models import News


# Register your models here.
class NewsAdmin(admin.ModelAdmin):

    model = News

    list_display = ('title',
                    'created_at',
                    'public_date',
                    'is_public')

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '150'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 10, 'cols': 150})},
    }


admin.site.register(News, NewsAdmin)
