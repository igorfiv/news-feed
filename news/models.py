from django.db import models
from django.utils import timezone

from .functions import generate_unique_name

# Create your models here.


class News(models.Model):
    title = models.CharField(max_length=255, default='')
    short_description = models.TextField(max_length=500, default='')
    text = models.TextField(default='')
    public_date = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    is_public = models.BooleanField(default=False)
    image = models.ImageField(upload_to=generate_unique_name, blank=True)
    keywords = models.CharField(max_length=255, default='', blank=True)

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'
